/*
\file GOST-28147-89.h
	\brief ������������ ���� � ��������� ����� ������, �������� � ���������� �������.
	�������� � ������������� ����� ������ � ��������. ��������� �������.
*/
#ifndef GOST-H
#define GOST_H
#include <conio.h>

//���� ������
typedef unsigned char BYTE;						//������ ������ ������������� ������
typedef unsigned long DWORD;					//32-� ������ ������������� ������

typedef union{
	BYTE parts[8];								//8-�� ������ ��� ������ ��� ��������� (64 ����)
	DWORD half[2];								//32-� ������ ��� ������ ��� ��������� (64 ����)
} GostPartData;					

//������� ������
#define _GostPartSize sizeof(GostPartData);		//������ ������ ��� ���������������
#define _GostImitSize sizeof(GostPartData);		//������ ������ ��� ������������
#define _GostSyncSize sizeof(GostPartData);		//������ ������ ��� �������������

#define _GostKeySize 32							//������ ����� � ������
#define _GostNextStep false						//��������� ��� ��������������������
#define _GostLastStep true						//��������� ��� ��������������������
	
#define _GostCompabilityStandart 1				//������������� � ��������� ����������
#define _GostCompabilityStandartSync 1			//������������� �������������

#define	_GostModeEncrypted true					//����� ����������
#define	_GostModeDecrypted false				//����� �������������

//������� ����������
void Gost_Imitta_Paste(BYTE *OpenData, BYTE *Imitta, DWORD Size, BYTE *GostTable, BYTE *GostKey);
void Gost_Simple_Replacement(BYTE *OpenData, DWORD Size, bool Mode, BYTE *GostTable, BYTE *GostKey);
void Gost_Prepare_Syncpack_Gamma(BYTE *Sync, BYTE *GostTable, BYTE *GostKey);
void Gost_Encrypted_Gamma(BYTE *Data, DWORD Size, BYTE *Syncpack, BYTE *GostTable, DWORD *GostKey);
void Gost_Encrypted_Gamma_With_Feedback(BYTE *Data, DWORD Size, BYTE *Sync, bool Mode, BYTE *GostTable);

#endif